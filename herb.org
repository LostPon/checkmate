#+TITLE: HERBIER
#+AUTHOR: checkmate
#+DATE: 2022-09-01
#+SETUPFILE: ~/Documents/org/org-notes-style/setup_html.org

* INTRODUCTION

* PLANTES
** Herbacées
*** Alliaires des Bois
/Alliaria petiolata/ - /Sisymbrium alliaria/

#+ATTR_HTML: :width 140px
[[./img/alliara-IMG_20180524_154550.jpg]]

#+ATTR_HTML: :width 140px
[[./img/alliara-IMG_20180524_154555.jpg]]

**** Identification
- feuilles larges, pétiolées, crénelées, grossièrement dentées, acuminées, 
- fleurs blanches en croix
- siliques ascendantes
- odeur d'ail feuilles froissées
  
**** Habitat
- annuelle pionière nitrophile des clairières et lisières
- rudéale
- hémisciaphile mésohydrique

**** Usage
- diurétique
- feuilles et racines consommables

*** Sceau de Salomon
/Polygonatum multiflorum/ - /Convallaria multiflora L./

#+ATTR_HTML: :width 320px
[[./img/Polygonatum_multiflorum.JPG]]

Plante vivace de 30-90cm, glabre
**** Identification
- tige cylindrique arquée
- feuilles alternes ovales ou oblongues, nervures convergentes
- fleurs blanches en clochette aux pointe vertes, suspendues sous la tige
**** Habitat
À l'ombre des bois, sols neutres plutôt argileux
-
** Arbres
** Arbustes
*** Sureau Noir
/Sambucus nigra/

#+ATTR_HTML: :width 140px
[[./img/Surea-noir-rameau.jpg]]

Arbre décidu
**** Identification
- branches souvent courbées
- écorce grise, lenticelles proéminentes
- feuilles (10-30cm) pari-pennées (5-7)
- baires noires en ombrelles pendantes
**** Habitat
**** Usage
